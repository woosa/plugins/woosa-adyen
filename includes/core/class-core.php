<?php
/**
 * Core
 *
 * @author Woosa Team
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class Core{


   /**
    * Clears cached payment methods.
    *
    * @since 1.1.1 - remove transient with a DB query instead of dedicated function
    *              - flush WP cache
    * @since 1.0.10
    * @return void
    */
   public static function clear_cached_payment_methods(){

      global $wpdb;

      $names = [
         PREFIX . '_is_active_',
         PREFIX . '_payment_method',
         PREFIX . '_stored_payment_methods_',
      ];

      foreach($names as $name){
         $wpdb->query("
            DELETE
               FROM `$wpdb->options`
            WHERE `option_name`
               LIKE ('_transient_$name%')
            OR `option_name`
               LIKE ('_transient_timeout_$name%')
         ");
      }

      wp_cache_flush();
   }



   /**
    * Updates cached payment methods.
    *
    * @since 1.0.10
    * @return void
    */
   public static function update_cached_payment_methods(){

      self::clear_cached_payment_methods();

      Service::checkout()->list_payment_methods();
   }



   /**
    * Gets the client IP
    *
    * @since 1.0.0
    * @return string
    */
   public static function get_client_ip(){

      $ip = '';

      if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
         $ip = $_SERVER['HTTP_CLIENT_IP'];
      } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
         $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      } else {
         $ip = $_SERVER['REMOTE_ADDR'];
      }

      return $ip;//'2.56.212.0'
   }



   /**
    * Gets local language.
    *
    * @return void
    */
   public static function get_locale(){

      if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
         $locale = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

         return $locale[0];
      }

      return str_replace('_', '-', get_locale());//get WP locale
   }

}