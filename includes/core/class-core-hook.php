<?php
/**
 * Core Hook
 *
 * @author Woosa Team
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class Core_Hook implements Interface_Hook{


   /**
    * Initiates the hooks.
    *
    * @return void
    */
   public static function init(){

      add_filter('woocommerce_payment_gateways', [__CLASS__, 'payment_gateway']);

      add_filter(PREFIX . '\logger\criteria_list', [__CLASS__, 'add_in_log_criteria_list']);

   }



   /**
    * Adds new gateway to WooCommerce payments.
    *
    * @since 1.0.0
    * @param array $gateways
    * @return array
    */
   public static function payment_gateway($gateways) {

      $gateways[] = Ideal::class;
      $gateways[] = Sepa_Direct_Debit::class;
      $gateways[] = Credit_Card::class;
      $gateways[] = Giropay::class;
      $gateways[] = Sofort::class;
      $gateways[] = Bancontact::class;
      $gateways[] = Bancontact_Mobile::class;
      $gateways[] = Boleto::class;
      $gateways[] = Alipay::class;
      $gateways[] = Wechatpay::class;
      $gateways[] = Googlepay::class;
      $gateways[] = Applepay::class;
      $gateways[] = Klarna::class;
      $gateways[] = Klarna_PayNow::class;
      $gateways[] = Klarna_Account::class;
      $gateways[] = Paypal::class;
      $gateways[] = DotPay::class;
      $gateways[] = Blik::class;
      $gateways[] = Vipps::class;
      $gateways[] = Swish::class;
      $gateways[] = Grabpay_MY::class;
      $gateways[] = Grabpay_PH::class;
      $gateways[] = Grabpay_SG::class;
      $gateways[] = Mobilepay::class;
      $gateways[] = MOLPay_ML::class;
      $gateways[] = MOLPay_TH::class;

      return $gateways;
   }



   /**
    * Insert log criteria for authorization access.
    *
    * @param array $items
    * @return array
    */
   public static function add_in_log_criteria_list($items){

      $items['version_update'] = [
         'type'    => 'warning',
         'message' => sprintf(
            __('Since v1.5.1 we will not deploy updates via our system anymore therefor you have to replace this version of the plugin with the version from Wordpress.org which can be found %shere%s.', 'woosa-adyen'),
            '<a href="https://wordpress.org/plugins/integration-adyen-woocommerce/">', '</a>'
         ),
         'hook'    => 'admin_init',
         'active'  => true,
      ];

      return $items;
   }


}